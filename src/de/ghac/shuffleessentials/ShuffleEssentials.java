package de.ghac.shuffleessentials;

import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import de.ghac.shuffleessentials.PlayerListener;

public class ShuffleEssentials extends JavaPlugin{
    
    /*Initialize Variables*/
    private Logger log = Logger.getLogger("Minecraft");
    Plugin pl = this;
    
    /*Message on disable*/
    @Override
    public void onDisable() {
        System.out.println("[" + this.getName() + "] Plugin deaktiviert!");
    }

    /*Message on enable
     *register listeners
     *add config
     */
    @Override
    public void onEnable() {
        System.out.println("[" + this.getName() + "] Plugin by ghac!");
        System.out.println("[" + this.getName() + "] Plugin aktiviert!");
        getServer().getPluginManager().registerEvents(new PlayerListener(pl), this);
        
        getConfig().addDefault("hideplayers.clearinv", true);
        getConfig().addDefault("hideplayers.star.name", "&4Spieler verstecken");
        getConfig().addDefault("hideplayers.star.lore", "&bLinksklicken, um alle Spieler zu verstecken%NL%&bRechtsklicken, um alle Spieler zu zeigen");
        getConfig().addDefault("hideplayers.messages.hideplayers", "&aAlle Spieler sind nun unsichtbar!");
        getConfig().addDefault("hideplayers.messages.showplayers", "&bAlle Spieler sind nun sichtbar!");
        getConfig().addDefault("hideplayers.messages.firework", "&cEin Feuerwerk wurde gestartet!");
        getConfig().addDefault("hideplayers.messages.noperm", "&4Du hast keine Berechtigung!");
        getConfig().addDefault("hideplayers.messages.announce.hideplayers", true);
        getConfig().addDefault("hideplayers.messages.announce.showplayers", true);
        getConfig().addDefault("hideplayers.messages.announce.firework", false);    
        getConfig().addDefault("hideplayers.messages.announce.noperm", true);    
        getConfig().options().copyDefaults(true);
        this.saveConfig();
        
    }
    
    
    public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args){
        /*Initialize Variables*/
        Player p = null;
        boolean consoleSender = false;
        
        /*Check for console as sender*/
        if(sender instanceof Player){
            p = (Player) sender;
        }else{
            consoleSender = true;
        }
        
        
            /*Command hideplayers*/
        if(cmd.getName().equalsIgnoreCase("hideplayers")){
            if(consoleSender){
                log.warning("Dieser Befehl kann nicht aus der Konsole benutzt werden!");
                return true;
            }
            /*Hide all players for player p*/
            HidePlayer.hideAllPlayers(p, pl);
            
            
            /*Command showplayers*/
        }else if(cmd.getName().equalsIgnoreCase("showplayers")){
            if(consoleSender){
                log.warning("Dieser Befehl kann nicht aus der Konsole benutzt werden!");
                return true;
            }
            /*Unhide all players for player p*/
            HidePlayer.unhideAllPlayers(p, pl);
            
            
            /*Command firework*/
        }else if(cmd.getName().equalsIgnoreCase("firework")){
            if(consoleSender){
                log.warning("Dieser Befehl kann nicht aus der Konsole benutzt werden!");
                return true;
            }
            
            if(!p.hasPermission("shuffleessentials.firework.deny")){
                /*Launch firework*/
                FireWork fw = new FireWork(p.getLocation(), pl);
                fw.sendFireWork();
                
                /*Send message*/
                if(getConfig().getBoolean("hideplayers.messages.announce.firework")){
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("hideplayers.messages.firework")));
                }
            }else{
                if(getConfig().getBoolean("hideplayers.messages.announce.noperm")){
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("hideplayers.messages.noperm")));
                }
            }
            
            
            /*Command shuffleessentials*/
        }else if(cmd.getName().equalsIgnoreCase("shuffleessentials")){
            if(args.length > 0){
                /*Subcommand reload*/
                if(args[0].equalsIgnoreCase("reload")){
                    /*Reload if console is performing*/
                    if(consoleSender){
                        reloadConfig();
                        log.warning("[" + this.getName() + "] Config reloaded.");
                        return true;
                    }else{
                        /*Reload if player is performing and player has permission shuffleessentials.reload*/
                        if(p.hasPermission("shuffleessentials.reload")){
                            reloadConfig();
                            p.sendMessage(ChatColor.GOLD + "[" + getName() +"] " +ChatColor.DARK_GREEN + "Config reloaded!");
                        }else{  
                            if(getConfig().getBoolean("hideplayers.messages.announce.noperm")){
                                p.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("hideplayers.messages.noperm")));
                            }
                        }
                    }
                }
            }else{
                /*Send information about plugin*/
                if(consoleSender){
                    log.warning("[" + this.getName() + "] ShuffleEssentials plugin by ghac. 2014.");
                    return true;
                }
                p.sendMessage(ChatColor.GOLD + "[" + getName() +"] " +ChatColor.DARK_GREEN + "ShuffleEssentials plugin by ghac. 2014.");
            }
        }
return true;
    }
    
    
}
