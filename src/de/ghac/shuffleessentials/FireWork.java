package de.ghac.shuffleessentials;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

public class FireWork {


    /*Initialize Variables*/
    private Location loc;
    private Plugin plugin;
    private Entity ent = null;
    
    /*Constructor*/
    public FireWork(Location l, Plugin pl){
        this.loc = new Location(l.getWorld(), l.getX(), l.getY(), l.getZ(),0 ,90);
        this.plugin = pl;
    }
    
    /*send firework every 10 ticks*
     *will look for better variant later
     */
    public void sendFireWork(){

        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            public void run() {
                shootFirework((double) 0.7, loc.add(0, 50, 0), false);
            }}, 0L);
        
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            public void run() {
                shootFirework((double) 0.7, loc.add(0, -8, 0), false);
            }}, 10L);
        
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            public void run() {
                shootFirework((double) 0.7, loc.add(0, -8, 0), false);
            }}, 20L);
        
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            public void run() {
                shootFirework((double) 0.7, loc.add(0, -8, 0), false);
            }}, 30L);
        
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            public void run() {
                shootFirework((double) 0.7, loc.add(0, -8, 0), false);
            }}, 40L);
        
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            public void run() {
                shootFirework((double) 0.7, loc.add(0, -8, 0), false);
            }}, 50L);
        
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            public void run() {
                shootFirework((double) 0.7, loc.add(0, -8, 0), true);
            }}, 60L);
       
        
    }
    
    /*launch Firework*/
    public void shootFirework(Double speed, Location shootLocation, boolean effect) {
        
        /*Let new rocket spawn on old rocket's position*/
        if(ent != null){
            shootLocation.setX(ent.getLocation().getX());
            shootLocation.setY(ent.getLocation().getY());
            shootLocation.setZ(ent.getLocation().getZ());
            ent.remove();
        }
            
    
        Vector directionVector = shootLocation.getDirection().normalize();
        double startShift = 2;
        Vector shootShiftVector = new Vector(directionVector.getX() * startShift, directionVector.getY() * startShift, directionVector.getZ() * startShift);
        shootLocation = shootLocation.add(shootShiftVector.getX(), shootShiftVector.getY(), shootShiftVector.getZ());
        

        Firework firework = shootLocation.getWorld().spawn(shootLocation, Firework.class);
        ent = (Entity) firework;
        
        /*If rocket is last rocket
         *add effects*/
        if(effect){
            FireworkMeta data = (FireworkMeta) firework.getFireworkMeta();
            data.addEffects(FireworkEffect.builder().withColor(Color.GREEN).with(Type.BALL_LARGE).build());
            data.addEffects(FireworkEffect.builder().withColor(Color.BLUE).with(Type.STAR).build());
            data.addEffects(FireworkEffect.builder().withColor(Color.PURPLE).with(Type.BURST).build());        
            firework.setFireworkMeta(data);
        }
        
        firework.setVelocity(directionVector.multiply(speed));
         
}
}
