package de.ghac.shuffleessentials;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import de.ghac.shuffleessentials.HidePlayer;
import de.ghac.shuffleessentials.Inventory;

public class PlayerListener implements Listener{
    /*Initialize variables*/
    Plugin pl = null;
    
    /*Constructor*/
    public PlayerListener(Plugin pl){
        this.pl = pl;
    }
    
    
    /*hide joined player for all players
     *add NetherStar to inventory
     */
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){
        for(Player p : HidePlayer.PlayersHidden){
            p.hidePlayer(e.getPlayer());
        }
        Inventory.addHideItem(e.getPlayer(), pl);
    }
    
    
    /*Remove quitting player from ArrayList*/
    @EventHandler
    public void onPlayerJoinLeave(PlayerQuitEvent e){
        if(HidePlayer.PlayersHidden.contains(e.getPlayer())){
            HidePlayer.PlayersHidden.remove(e.getPlayer());
        }
    }
    
    
    /*Cancel InventoryMove
     *if item is NetherStar
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerClickInventory(InventoryClickEvent e){
        ItemStack is = e.getCurrentItem();
        try{
            if(is.hasItemMeta() && is.getType().equals(Material.NETHER_STAR)){
                String name = is.getItemMeta().getDisplayName();
                if(name.equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', pl.getConfig().getString("hideplayers.star.name")))){
                    e.setCancelled(true);
                }
            }
        }catch(NullPointerException ex){}
    }
    
    /*Cancel DropItem
     *if item is NetherStar
     */   
    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent e){
        ItemStack is = e.getItemDrop().getItemStack();
        if(is.hasItemMeta() && is.getType().equals(Material.NETHER_STAR)){
            String name = is.getItemMeta().getDisplayName();
            if(name.equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', pl.getConfig().getString("hideplayers.star.name")))){
                e.setCancelled(true);
            }
        }
    }
        
    /*Hide / Unhide players on click
     *if item is NetherStar
     */
    @EventHandler
    public void onPlayerClickStar(PlayerInteractEvent e){
        if(e.getAction().equals(Action.LEFT_CLICK_AIR) || e.getAction().equals(Action.LEFT_CLICK_BLOCK)){
            ItemStack is = e.getPlayer().getItemInHand();
            if(is.hasItemMeta() && is.getType().equals(Material.NETHER_STAR)){
                String name = is.getItemMeta().getDisplayName();
                if(name.equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', pl.getConfig().getString("hideplayers.star.name")))){
                    HidePlayer.hideAllPlayers(e.getPlayer(), pl);
                }
            }
        }else if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
            ItemStack is = e.getPlayer().getItemInHand();
            if(is.hasItemMeta() && is.getType().equals(Material.NETHER_STAR)){
                String name = is.getItemMeta().getDisplayName();
                if(name.equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', pl.getConfig().getString("hideplayers.star.name")))){
                    HidePlayer.unhideAllPlayers(e.getPlayer(), pl);
                }
            }
        }
    }
    
    
    
}
