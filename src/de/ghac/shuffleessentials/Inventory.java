package de.ghac.shuffleessentials;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

public class Inventory {

    /*Add item for hiding players on join
     *if hideplayers.clearinv is true, delete inventory on join
     *
     */
    public static void addHideItem(Player p, Plugin pl){
        PlayerInventory inv = p.getInventory();
        
        ItemStack is = new ItemStack(Material.NETHER_STAR);
        ItemMeta im = is.getItemMeta();
        
        String name = ChatColor.translateAlternateColorCodes('&', pl.getConfig().getString("hideplayers.star.name"));
        String lore = ChatColor.translateAlternateColorCodes('&', pl.getConfig().getString("hideplayers.star.lore"));

        ArrayList<String> lorelist = new ArrayList<String>();
        
        String[] lorearray = lore.split("%NL%", -1); //Split in lines

        for(int i = 0; i < lorearray.length; i++){
        lorelist.add(lorearray[i]);
        }
        
        im.setDisplayName(name);
        im.setLore(lorelist);
        
        if(pl.getConfig().getBoolean("hideplayers.clearinv")){
            inv.clear();
        }
        
        is.setItemMeta(im);
        inv.addItem(is);
    }
}
