package de.ghac.shuffleessentials;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class HidePlayer{
    
    /*Initialize Variables*/
    public static ArrayList<Player> PlayersHidden = new ArrayList<Player>();

    
    /*hide all players for player p without permission
     *shuffleessentials.hideplayers.overwrite
     */
    public static void hideAllPlayers(Player p, Plugin pl){
        if(!p.hasPermission("shuffleessentials.hideplayers.deny")){
            if (!(PlayersHidden.contains(p))){
                PlayersHidden.add(p);
            }
            
            for(Player onlineplayer : Bukkit.getOnlinePlayers()){
                if(!(p.equals(onlineplayer))){
                    if(!onlineplayer.hasPermission("shuffleessentials.hideplayers.overwrite")){
                        p.hidePlayer(onlineplayer);
                    }
                }
            }
            if(pl.getConfig().getBoolean("hideplayers.messages.announce.hideplayers")){
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', pl.getConfig().getString("hideplayers.messages.hideplayers")));
            }
         }else{
            if(pl.getConfig().getBoolean("hideplayers.messages.announce.noperm")){
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', pl.getConfig().getString("hideplayers.messages.noperm")));
            }
        }
    }
    
    
    /*hide one player for player if p hasn't permission 
     *shuffleessentials.hideplayers.overwrite 
     */
    public static void hidePlayer(Player p, Player hide, Plugin pl){
        if(!p.hasPermission("shuffleessentials.hideplayers.deny")){
            if(!hide.hasPermission("shuffleessentials.hideplayers.overwrite")){
            p.hidePlayer(hide);
            }else{
                if(pl.getConfig().getBoolean("hideplayers.messages.announce.noperm")){
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', pl.getConfig().getString("hideplayers.messages.noperm")));
                }
            }
        }
    }
    
    
    /*unhide all players for player p*/
    public static void unhideAllPlayers(Player p, Plugin pl){
        if(!p.hasPermission("shuffleessentials.hideplayers.deny")){
            if(PlayersHidden.contains(p)){
            for(Player onlineplayer : Bukkit.getOnlinePlayers()){
                if(!(p.equals(onlineplayer))){
                    p.showPlayer(onlineplayer);
                    }
                }
            PlayersHidden.remove(p);
            }
            if(pl.getConfig().getBoolean("hideplayers.messages.announce.showplayers")){
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', pl.getConfig().getString("hideplayers.messages.showplayers")));
            }
        }else{
            if(pl.getConfig().getBoolean("hideplayers.messages.announce.noperm")){
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', pl.getConfig().getString("hideplayers.messages.noperm")));
            }
        }
   }
    
    
}
